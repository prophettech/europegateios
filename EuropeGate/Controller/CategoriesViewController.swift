//
//  RestaurantListingTableViewController.swift
//  Food Delivery
//
//  Created by Akram Hussain on 28/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit

class CategoriesViewController: UITableViewController {
    
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet var productsTableView: UITableView!
    @IBOutlet weak var cartButton: UIBarButtonItem!
    
    var categories = Categories()
    var filteredCategories: Categories?
    let activityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.black
        getCategories()
    }
    
    func getCategories(){
        showActivityIndicator()
        Services.getAllCategories { (categories) in
            let temp = categories
            for i in temp! {
                if i.count != 0 {
                    self.categories.append(i)
                    self.categories = self.categories.sorted(by: { $0.menuOrder < $1.menuOrder })
                }
            }
            self.productsTableView.dataSource = self
            self.productsTableView.delegate = self
            self.tableView.reloadData()
            self.hideActivityIndicator()
        }
    }
    
    @IBAction func cartAction(_ sender: Any) {
        let vc : CartDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cart") as! CartDetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showActivityIndicator()
    {
        activityIndicatorView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicatorView.center = tableView.center
        if #available(iOS 13.0, *) {
            activityIndicatorView.style = .large
        }
        activityIndicatorView.color = .red
        view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    func hideActivityIndicator()
    {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.removeFromSuperview()
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if SearchBar.text != "" {
            return filteredCategories?.count ?? 0
        }
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell", for: indexPath) as! CategoriesCell
        cell.categories = categories[indexPath.row]
        if SearchBar.text != "" {
            cell.categories = self.filteredCategories?[indexPath.row]
        }
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc : ProductsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProductsVC") as! ProductsViewController
        vc.categoryID = String((categories[indexPath.row].id)!)
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension CategoriesViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredCategories = self.categories.filter({ (category) -> Bool in
            return category.name?.lowercased().range(of: searchText.lowercased()) != nil
        })
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

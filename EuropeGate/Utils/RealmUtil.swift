//
//  RealmUtil.swift
//  Food Delivery
//
//  Created by Akram Hussain on 09/01/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation
import RealmSwift

class RealmUtil{
    
    class func setCartItem(cartItem: CartItem){
        var cartItems = List<CartItem>()
        var exist = false
        var cart = uiRealm.object(ofType: Cart.self, forPrimaryKey: 101)
        if cart != nil && (cart?.products.count)! > 0{
            cartItems = cart!.products
            exist = true
            uiRealm.beginWrite()

        }else {
            cart = Cart()
        }
        let item = uiRealm.object(ofType: CartItem.self, forPrimaryKey: cartItem.productId)
        if item != nil && item!.productId != "" {
            if item?.productId == cartItem.productId {
                item?.quantity = cartItem.quantity
                item?.price = cartItem.price
            }
        }else {
            cartItems.append(cartItem)
        }
        cart?.products = cartItems
        
        if exist {
            try! uiRealm.commitWrite()
        }else {
            try! uiRealm.write {
                uiRealm.add(cart!, update: .all)
            }
        }
    }
    
    class func getCartItems(completion: @escaping(Cart)-> Void){
        let realm = try! Realm()
        let cart = realm.object(ofType: Cart.self, forPrimaryKey: 101) ?? Cart()
        if (cart != nil) {
            completion(cart)
        }
    }
    
    class func updateCart(cart: Cart){
        let realm = try! Realm()
        let realmCart = realm.object(ofType: Cart.self, forPrimaryKey: 101) ?? Cart()
        var exist = false
        if (realmCart.products.count > 0){
            realm.beginWrite()
            exist = true
            realmCart.products = cart.products
            realmCart.totalQuantity = cart.totalQuantity
            realmCart.totalPrice = cart.totalPrice
        }
        if exist{
            try! realm.commitWrite()
        }else {
            try! realm.write {
                realm.add(realmCart, update: .all)
            }
        }
    }
    
    class func deleteItem(cartItem: CartItem){
        if let item = uiRealm.object(ofType: CartItem.self, forPrimaryKey: cartItem.productId){
            try! uiRealm.write {
                uiRealm.delete(item)
            }
        }
    }
}

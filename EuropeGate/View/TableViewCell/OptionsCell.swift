//
//  OptionsCell.swift
//  Food Delivery
//
//  Created by Akram Hussain on 05/12/2019.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import UIKit

protocol OptionSelected {
    func optionSelected(title: String, price: String, selected: Bool)
}

class OptionsCell: UITableViewCell {

    @IBOutlet weak var optionPrice: UILabel!
    @IBOutlet weak var optionTitle: UILabel!
    @IBOutlet weak var optionSwitch: UISwitch!
    var delegate: OptionSelected?
    
    @IBAction func optionSelected(_ sender: Any) {
        delegate?.optionSelected(title: optionTitle!.text!, price: (optionPrice.text?.replacingOccurrences(of: " ￡", with: ""))!, selected: optionSwitch.isOn)
    }
}

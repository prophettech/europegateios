//
//  Cart.swift
//  Food Delivery
//
//  Created by Akram Hussain on 28/11/19.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation
import RealmSwift

class Cart: Object {
    
    @objc dynamic var primaryKey = 101
    var products = List<CartItem>()
    @objc dynamic var totalQuantity = Int()
    @objc dynamic var totalPrice = Double()
    
    
    override class func primaryKey() -> String? {
        return "primaryKey"
    }

//    func writeCartToRealm(){
//        try! uiRealm.write {
//            uiRealm.add(self, update: true)
//        }
//    }
}


//
//  CartItem.swift
//  Food Delivery
//
//  Created by Akram Hussain on 07/12/2019.
//  Copyright © 2019 Akram Hussain. All rights reserved.
//

import Foundation
import RealmSwift

class CartItem : Object{
    
    @objc dynamic var productId = String()
    @objc dynamic var variationIds = Double()
    @objc dynamic var quantity = Int()
    @objc dynamic var name = String()
    @objc dynamic var price = Double()
    @objc dynamic var originalPrice = Double()
    @objc dynamic var selectedOptions = String()
    
    override class func primaryKey() -> String? {
        return "productId"
    }
}

extension CartItem{
    
//    func writeCartItemToRealm(){
//        try! uiRealm.write {
//            uiRealm.add(self)
//            uiRealm.add(self, update: true)
//        }
//    }
}

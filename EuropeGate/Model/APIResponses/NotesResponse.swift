//
//  NotesResponse.swift
//  Food Delivery
//
//  Created by Akram Hussain on 07/02/2020.
//  Copyright © 2020 Akram Hussain. All rights reserved.
//

import Foundation

struct NotesResponse: Codable {
    let id: Int
    let author, dateCreated, dateCreatedGmt, note: String
    let customerNote: Bool

    enum CodingKeys: String, CodingKey {
        case id, author
        case dateCreated = "date_created"
        case dateCreatedGmt = "date_created_gmt"
        case note
        case customerNote = "customer_note"
    }
}
